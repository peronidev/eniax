import pandas as pd
import numpy as np
from lagmatrix import lagmatrix


def load_data(file_name, column, num_input):
    try:
        data = pd.read_csv(file_name, sep=",")
        np.save("max_value.npy", data[column].max())
        data = data[column] / data[column].max()
        data_lag = lagmatrix(data, num_input)
        large = data_lag.shape[0]
        large = np.ceil(large * 0.8)
        data_training_x = data_lag[:int(large), :num_input]
        data_training_y = np.vstack(data_lag[:int(large), -1])
        data_testing_x = data_lag[int(large):, :num_input]
        data_testing_y = np.vstack(data_lag[int(large):, -1])

        training = {'x': data_training_x, 'y': data_training_y}
        testing = {'x': data_testing_x, 'y': data_testing_y}
        return training, testing
    except Exception as e:
        print(e)
