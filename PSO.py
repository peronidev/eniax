# -*- coding: utf-8 -*-
import numpy as np
from load_data import load_data
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error


def sigmoid(x):
    '''Función de activación'''
    return 1.0/(1 + np.exp(-x))


class Particle:
    def __init__(self, x, y, num_input, num_hidden_nodes, num_output):
        '''Inicialización de la partícula'''
        self.num_input = num_input
        self.num_hidden_nodes = num_hidden_nodes
        self.num_output = num_output
        self.input = x                      # Entrada de la red
        self.hidden_nodes = np.random.rand(self.num_input, self.num_hidden_nodes)       # Pesos ocultos
        self.output_nodes = np.random.rand(self.num_hidden_nodes, self.num_output)      # Pesos de salida
        self.best_hidden_nodes = np.zeros(self.hidden_nodes.shape)                      # Mejores pesos ocultos
        self.best_output_nodes = np.zeros(self.output_nodes.shape)                      # Mejores pesos de salida
        self.y = y                          # Salida esperada de la red
        self.output = np.zeros(y.shape)     # Salida de la red
        self.best_fitness = 10**10          # Mejor fitness
        self.fitness = 10**10               # Fitness
        self.hidden_velocity = np.random.rand(self.num_input, self.num_hidden_nodes)    # Velocidad para los pesos ocultos
        self.output_velocity = np.random.rand(self.num_hidden_nodes, self.num_output)   # Velocidad para los pesos de salida

    def feedforward(self):
        '''Método para calcular la salida de la red y el fitness'''
        self.hidden_layer = sigmoid(np.dot(self.input, self.hidden_nodes))
        self.output = sigmoid(np.dot(self.hidden_layer, self.output_nodes))
        self.fitness = mean_squared_error(self.y, self.output)

    def update_velocity(self, best_global):
        '''Método para actualizar la velocidad de la partícula'''
        w = 0.8                     # Coeficiente de inercia
        c1 = 1.2                    # Coeficiente cognitivo
        c2 = 0.8                    # Coeficiente social
        r1 = np.random.random()     # Variación de la velocidad cognitiva
        r2 = np.random.random()     # Variación de la velocidad social

        # v(t+1) = w*v(t) + c1*r1*[best_position(t) - position(t)] + c2*r2*[best_global(t) - position(t)]
        self.hidden_velocity = w*self.hidden_velocity + c1*r1*(self.best_hidden_nodes - self.hidden_nodes) + c2*r2*(best_global.hidden_nodes - self.hidden_nodes)
        self.output_velocity = w*self.output_velocity + c1*r1*(self.best_output_nodes - self.output_nodes) + c2*r2*(best_global.output_nodes - self.output_nodes)

    def update_position(self):
        '''Método para actualizar la posición de la partícula'''
        self.hidden_nodes = self.hidden_nodes + self.hidden_velocity
        self.output_nodes = self.output_nodes + self.output_velocity

    def update_best_position(self):
        '''Método para determinar la mejor posición de la partícula'''
        if self.fitness <= self.best_fitness:
            self.best_fitness = self.fitness
            self.best_hidden_nodes = self.hidden_nodes
            self.best_output_nodes = self.output_nodes


if __name__ == "__main__":
    # Parámetros de la red
    num_input = 4           # Número de entradas
    num_hidden_nodes = 5    # Número de nodos ocultos
    num_output = 1          # Número de salidas
    num_iterations = 2000   # Número de iteraciones
    num_particles = 15      # Número de partículas del enjambre
    swarm = []              # Enjambre
    best_global = None      # Mejor global
    errors = []             # Lista de fitness

    # Comentar/descomentar para utilizar uno u otro archivo
    # training_data, testing_data = load_data("datos.csv", "Fondo", num_input)
    training_data, testing_data = load_data("meteo.csv", "Temperatura", num_input)

    # Inicialización del enjambre
    for i in range(num_particles):
        swarm.append(Particle(training_data.get("x"), training_data.get("y"), num_input, num_hidden_nodes, num_output))

    # -- Entrenamiento de la red --
    # PSO
    for i in range(num_iterations):
        for j in range(num_particles):
            # Actualizar el mejor global
            if best_global is None or swarm[j].fitness <= best_global.fitness:
                best_global = swarm[j]
            errors.append(best_global.fitness)

            swarm[j].feedforward()                  # Calcula la salida de la red y el fitness
            swarm[j].update_velocity(best_global)   # Actualiza la velocidad de la partícula
            swarm[j].update_position()              # Actualiza la posición de la partícula
            swarm[j].update_best_position()         # Actualiza la mejor posición de la partícula

        print("Iteration : {0} - MSE : {1}".format(i, best_global.fitness))

    # Imprime el mejor fitness y los pesos de la red
    print(best_global.fitness)
    print(best_global.hidden_nodes)
    print(best_global.output_nodes)

    # Grafica el error en cada iteración
    plt.plot(errors)
    plt.show()

    # Grafica la salida esperada vs la salida de la red
    plt.plot(training_data.get("y"))
    plt.plot(best_global.output)
    plt.show()

    # -- Testing de la red --
    best_global.input = testing_data.get("x")
    best_global.y = testing_data.get("y")
    best_global.feedforward()
    print("MSE : {0}".format(best_global.fitness))

    # Grafica la salida esperada vs la salida de la red
    plt.plot(testing_data.get("y"))
    plt.plot(best_global.output)
    plt.show()

    # Guarda los pesos de la red
    # np.save("hidden_nodes.npy", best_global.hidden_nodes)
    # np.save("output_nodes.npy", best_global.output_nodes)
