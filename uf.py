# -*- coding: utf-8 -*-
import requests
import locale
import calendar
from datetime import datetime


def get_uf(year):
    try:
        response = requests.get("https://api.sbif.cl/api-sbifv3/recursos_api/uf/{0}?apikey=3b157b4425972e0d522eeb6875e1c8fc02572cee&formato=json".format(year))
        return response.json()
    except Exception as e:
        print(e)
        return None


if __name__ == '__main__':

    data = get_uf(2018)

    if data is not None:
        # Cambio de idioma
        locale.setlocale(locale.LC_ALL, 'es_ES.utf8')

        # Diccionario para almacenar los datos por mes
        months = {}
        [months.update({x: []}) for x in range(1, 13)]

        for row in data['UFs']:
            months[datetime.strptime(row['Fecha'], '%Y-%m-%d').month].append({
                'Fecha': datetime.strptime(row['Fecha'], '%Y-%m-%d').strftime("%d-%m-%Y"),
                'Valor': float(row['Valor'].replace('.', '').replace(',', '.'))
            })

        # Definición de variables
        increased = []
        decreased = []
        maintained = []
        max_uf = max([j for i in months.values() for j in i], key=lambda x: x['Valor'])
        min_uf = min([j for i in months.values() for j in i], key=lambda x: x['Valor'])

        # Proceso para determinar si aumentó, disminuyó o se mantuvo valor UF
        for i in range(1, 13):
            months[i].sort(key=lambda x: x['Fecha'])
            variance = round(months[i][-1]['Valor'] - months[i][0]['Valor'], 2)
            variance_dict = {'Mes': calendar.month_name[i].capitalize(), 'Valor': variance*-1 if variance < 0 else variance}
            if variance > 0:
                increased.append(variance_dict)
            elif variance < 0:
                decreased.append(variance_dict)
            else:
                maintained.append(variance_dict)

        increased.sort(key=lambda x: x['Valor'])
        decreased.sort(key=lambda x: x['Valor'], reverse=True)
        maintained.sort(key=lambda x: x['Mes'])
        print("Meses en que aumentó el valor de la UF: {0}".format(increased))
        print("Meses en que disminuyó el valor de la UF: {0}".format(decreased))
        print("Meses en que se mantuvo el valor de la UF: {0}".format(maintained))
        print("Día con mayor valor UF: {0}, valor UF: {1}".format(max_uf['Fecha'], max_uf['Valor']))
        print("Día con menor valor UF: {0}, valor UF: {1}".format(min_uf['Fecha'], min_uf['Valor']))
